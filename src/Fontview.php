<?php

namespace Smtm\Fontview;

use FontLib\Font;
use Imagick;
use ImagickDraw;
use ImagickPixel;

class Fontview
{
    private Imagick $imagick;
    private ImagickDraw $imagickDraw;
    private ImagickPixel $imagickPixel;
    private ImagickPixel $imagickBackground;
    private ImagickPixel $imagickTextColor;

    public function __construct()
    {
        $this->imagick = new Imagick();
        $this->imagickDraw = new ImagickDraw();
        $this->imagickPixel = new ImagickPixel();
        $this->imagickBackground = new ImagickPixel();
        $this->imagickTextColor = new ImagickPixel();
    }

    public function renderGlyph(string $code, string $fontPath)
    {
        $this->imagick->clear();
        $this->imagickDraw->clear();
        $this->imagickPixel->clear();
        $this->imagickBackground->clear();
        $this->imagickTextColor->clear();

        try {
            //$this->imagickPixel->setColor('rgba(0, 0, 0, 255)');
            $this->imagickPixel->setColor('black');
            $this->imagickDraw->setFillColor($this->imagickPixel);
            $this->imagickDraw->setFontSize('30');
            //$this->imagickDraw->setTextAlignment(Imagick::ALIGN_CENTER);
            $this->imagickDraw->setGravity(Imagick::GRAVITY_CENTER);
            $this->imagickDraw->setFont($fontPath);
            $this->imagickDraw->annotation(
                0,
                0,
                mb_convert_encoding(
                    '&#' . $code . ';',
                    //str_repeat('&#' . hexdec('f017') . ';', 10),
                    'UTF-8',
                    'HTML-ENTITIES'
                )
            );

            $this->imagick->newImage(40, 40, 'rgba(255, 255, 255, 0)');
            //$this->imagick->annotateImage($this->imagickDraw, 500, 400, 0, '`1234567890-=~!@#$%^&*()_+asdfghjkl;\'ASDFGHJKL:"zxcvbnm,./ZXCVBNM<>?"чЧ№€§явертъуиопшщЯВЕРТЪУИОПШЩасдфгхйклАСДФГХЙКЛзьцжбнмЗѝЦЖБНМ');
            /*
            $this->imagick->annotateImage(
                $this->imagickDraw,
                250,
                200,
                0,
                mb_convert_encoding(
                    str_repeat('&#' . hexdec('f017') . ';', 10),
                    'UTF-8',
                    'HTML-ENTITIES'
                )
            );
            */
            /*
            $this->imagick->setGravity(Imagick::GRAVITY_CENTER);
            $this->imagick->annotateImage(
                $this->imagickDraw,
                0,
                0,
                0,
                mb_convert_encoding(
                    str_repeat('&#' . hexdec('f017') . ';', 10),
                    'UTF-8',
                    'HTML-ENTITIES'
                )
            );
            */
            $this->imagick->setGravity(Imagick::GRAVITY_CENTER);
            $this->imagick->drawImage($this->imagickDraw);
            $this->imagick->setImageFormat('png');
        } catch (\Throwable $t) {
            var_dump($t);
        }

        return 'data:image/png;base64,' . base64_encode($this->imagick->getImagesBlob());
    }

    public function renderGlyphsHtml(string $fontPath, ?string $stylesheetPath = null, ?bool $fullClassNames = null)
    {
        $stylesheet = null;
        $allClassesIndexed = [];
        if ($stylesheetPath !== null) {
            $stylesheet = file_get_contents($stylesheetPath);

            /**
             * Finds all classes which define a "content" rule like:
             * .myClass {content: "\abcd";}
             */
            if (preg_match_all('#\.([^{]+?){(?:\s|\R)*?content:\s*["\']\\\\([a-fA-F0-9]*?)["\']\s*?(?:(?:!important)?;)\s*?(?:\s|\R)*?}#i', $stylesheet, $matches)) {
                $allClassesIndexed = array_change_key_case(array_combine($matches[2], $matches[1]), CASE_LOWER);
            }
        }

        $font = Font::load($fontPath);
        $unicodeCharmap = $font->getUnicodeCharMap();

        $output = [];
        foreach (array_keys($unicodeCharmap) as $code) {
            $dechex = dechex($code);
            $stylesheetClass = '';
            /**
             * This matches only the particular class using the dechex content value
             */
            if ($stylesheet !== null && preg_match('#\.([^{]+?){(?:\s|\R)*?content:\s*["\']\\\\' . $dechex . '["\']\s*?(?:(?:!important)?;)\s*?(?:\s|\R)*?}#i', $stylesheet, $match)) {
                $stylesheetClass = '';
                if ($fullClassNames) {
                    $stylesheetClass .= $match[1];
                } else {
                    $classes = preg_split('#\s*,\s*#', $match[1]);
                    $stylesheetClass .= str_replace(':before', '', reset($classes));
                }
                $stylesheetClass .= '<br/>';
            }
            $imageData = $this->renderGlyph($code, $fontPath);
            $output[] = <<< EOT
            <div style="display: inline-block; box-sizing: border-box; padding: 2px; border: 1px solid #444; text-align: center;">
                $stylesheetClass$code
                <br/>$dechex
                <br/><img src="$imageData" alt=""/>
            </div>
            EOT;
        }

        return implode($output);
    }
}
